package com.example.tugasakhir;

public class Berita {
    public String imageURL, judul, deskripsi;

    public Berita(){

    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Berita(String imageURL, String judul, String deskripsi){
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.imageURL = imageURL;
    }
}
