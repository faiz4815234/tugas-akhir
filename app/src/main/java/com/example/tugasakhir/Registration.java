package com.example.tugasakhir;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Registration extends AppCompatActivity {
    EditText editTextFullName, editTextEmail, editTextPhoneNumber, editTextPassword, editTextConfPassword;
    Button buttonReg;
    FirebaseAuth mAuth;
    ProgressBar progressBar;
    TextView textView;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    SharedPreferences preferences;

    @Override
    public void onStart() {
        super.onStart();
        Log.d("Registration", "currentStatus = " + UserStatus.status.name());
        preferences = getSharedPreferences("status", MODE_PRIVATE);
        String status = preferences.getString("status_login", "");
        if(status.equals(Status.LOGIN.name())){
            Intent intent = new Intent(getApplicationContext(), (MainActivity.class));
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mAuth = FirebaseAuth.getInstance();
        editTextFullName = findViewById(R.id.editFullName);
        editTextEmail = findViewById(R.id.editEmail);
        editTextPhoneNumber = findViewById(R.id.editPhone);
        editTextPassword = findViewById(R.id.editPass);
        editTextConfPassword = findViewById(R.id.editConfPass);
        buttonReg = findViewById(R.id.buttonConfirmPass);
        progressBar = findViewById(R.id.progressBar);
        textView = findViewById(R.id.login);
        textView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
//                finish();
            }
        });

        buttonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Registration", "Sebelum Visible Progress Bar");
                progressBar.setVisibility(View.VISIBLE);
                Log.d("Registration", "Sesudah Visible Progress Bar");
                String email, password;
                email = String.valueOf(editTextEmail.getText());
                password = String.valueOf(editTextPassword.getText());

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(Registration.this, "Enter email", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(Registration.this, "Enter password", Toast.LENGTH_SHORT).show();
                    return;
                }

                String fullName, phoneNumber;
                fullName = String.valueOf(editTextFullName.getText());
                phoneNumber = String.valueOf(editTextPhoneNumber.getText());

                Map<String, Object> user = new HashMap<>();
                user.put("Full Name", fullName);
                user.put("Email", email);
                user.put("Password", password);
                user.put("Phone Number", phoneNumber);

                db.collection("users")
                        .add(user)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding document", e);
                            }
                        });


                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Log.d("Registration", "Sebelum Gone Progress Bar");
                                progressBar.setVisibility(View.GONE);
                                if (task.isSuccessful()) {
                                    Toast.makeText(Registration.this, "Account created.", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), Login.class);
                                    Log.d("Registration", "finished");
                                    preferences = getSharedPreferences("status", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("status_login", Status.REGISTER.name());
                                    editor.apply();
                                    startActivity(intent);
//                                    finish();
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Toast.makeText(Registration.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .addOnFailureListener(command -> {
                            Log.e("Registration", "error occurred with message " + command.getMessage());
                        });
            }
        });
    }
}
