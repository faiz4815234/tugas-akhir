package com.example.tugasakhir;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tugasakhir.R;

import java.util.ArrayList;
import java.util.List;

public class InputAdapter extends RecyclerView.Adapter<InputAdapter.BeritaViewHolder> {

    private ArrayList<Berita> berita;
    private Context context;

    public InputAdapter(ArrayList<Berita> berita, Context context) {
        this.berita = berita;
        this.context = context;
    }

    @NonNull
    @Override
    public BeritaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.input_adapter, parent, false);
        return new BeritaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BeritaViewHolder holder, int position) {
        Glide.with(context).load(berita.get(position).getImageURL()).into(holder.recycleImage);
        holder.recycleJudul.setText(berita.get(position).getJudul());
    }

    @Override
    public int getItemCount() {
        return berita.size();
    }

    public class BeritaViewHolder extends RecyclerView.ViewHolder {
        ImageView recycleImage;
        TextView recycleJudul;
        ImageButton tripleDot;

        public BeritaViewHolder(@NonNull View itemView) {
            super(itemView);

            recycleImage = itemView.findViewById(R.id.imageView);
            recycleJudul = itemView.findViewById(R.id.tvJudul);
            tripleDot = itemView.findViewById(R.id.buttonPopUp);

            tripleDot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(context, tripleDot); // Use 'context' as the first argument
                    popupMenu.getMenuInflater().inflate(R.menu.icon_menu, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            final int menuEdit = R.id.menuEdit;
                            final int menuDelete = R.id.menuDelete;
                            if(item.getItemId() == menuEdit){
                                Toast.makeText(context, "Ini adalah menu edit", Toast.LENGTH_SHORT).show();
                            } else if(item.getItemId() == menuDelete){
                                Toast.makeText(context, "Ini adalah menu delete", Toast.LENGTH_SHORT).show();
                            }
                            return true;
                        }
                    });
                    popupMenu.show(); // Show the PopupMenu
                }
            });
        }
    }
}
